﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BucketSorting
{
    public static class ListExt
    {
        public static void AddSorted<T>(this List<T> @this, T item) where T : IComparable<T>
        {
            if (@this.Count == 0)
            {
                @this.Add(item);
                return;
            }

            if (@this[@this.Count - 1].CompareTo(item) <= 0)
            {
                @this.Add(item);
                return;
            }

            if (@this[0].CompareTo(item) >= 0)
            {
                @this.Insert(0, item);
                return;
            }

            int index = @this.BinarySearch(item);
            if (index < 0)
                index = ~index;
            @this.Insert(index, item);
        }
    }

    public class Item : IComparable<Item>
    {
        public int Value { get; set; }

        public int CompareTo(Item other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(null, other)) return 1;
            return Value.CompareTo(other.Value);
        }
    }

    public class Bucket
    {
        private const int DividedFactor = 10;
        private List<List<Item>> _items;
        private const int _capacity = 10;

        public Bucket()
        {
            _items = new List<List<Item>>(Enumerable.Repeat(default(List<Item>), _capacity));
        }

        public Bucket(List<Item> items) : this()
        {
            foreach (var item in items)
            {
                AddItem(item);
            }
        }

        public void AddItem(Item item)
        {
            int itemValue = item.Value;
            int index = Math.Min(itemValue / DividedFactor, itemValue + 1 / DividedFactor);

            if (_items[index] == null)
                _items[index] = new List<Item>();

            _items[index].AddSorted(item);
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var list in _items)
            {
                list?.ForEach(item => { stringBuilder.Append(item.Value).Append("-"); });
            }

            return stringBuilder.ToString();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            List<Item> items = new List<Item>()
            {
                new Item() {Value = 65},
                new Item() {Value = 64},
                new Item() {Value = 63},
                new Item() {Value = 54},
                new Item() {Value = 4},
            };
            Bucket bucket = new Bucket(items);
            Console.WriteLine(bucket);
        }
    }
}